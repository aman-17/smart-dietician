from flask import Blueprint, render_template, request, flash, jsonify
from flask_login import login_required, current_user
from .models import Note
from . import db
import json


views = Blueprint('views', __name__)


@views.route('/', methods=['GET', 'POST'])
@login_required
def home():
    if request.method == 'POST':
        age = request.form.get('age')
        gender = request.form.get('gender')
        user_name = request.form.get('firstName1')
        height = request.form.get('height')
        weight = request.form.get('weight')
        activity_level = request.form.get('activity')
        weight_goal = request.form.get('goal')

    return render_template("home.html", user=current_user)

